Contains hooks that a relevant to most projects

* post-merge: Will run `composer install` if a change to composer.lock is merged after `git merge` or `git pull`

# Usage

`git clone <this repo> ~/git_hook_templates`

`cd /path/to/your/repo`

Install the hook(s) - Note, this will overwrite hooks that already exist.

`cp ~/git_hook_templates/hooks/* .git/hooks/`
